/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class Vendedor extends Empleado{
    // Atributos
    private String telMovil, areaVenta;
    
    public double increme = 0.10;
    private Vehiculo vehiculo;
    private List<String> clientes = new ArrayList<>();

    public Vendedor() {
    }

    public Vendedor(String telMovil, String areaVenta, Vehiculo vehiculo, String nombre, String apellidos, String cedula, String direccion, int tiempoLabora, int telefono, int salario) {
        super(nombre, apellidos, cedula, direccion, tiempoLabora, telefono, salario);
        this.telMovil = telMovil;
        this.areaVenta = areaVenta;
        
        this.vehiculo = vehiculo;
    }

    public String getTelMovil() {
        return telMovil;
    }

    public void setTelMovil(String telMovil) {
        this.telMovil = telMovil;
    }

    public String getAreaVenta() {
        return areaVenta;
    }

    public void setAreaVenta(String areaVenta) {
        this.areaVenta = areaVenta;
    }


    public double getIncreme() {
        return increme;
    }

    public void setIncreme(double increme) {
        this.increme = increme;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public List<String> getClientes() {
        return clientes;
    }

    public void setClientes(List<String> clientes) {
        this.clientes = clientes;
    }

    

    @Override
    public String toString() {
        return "Vendedor{" + "telMovil=" + telMovil + ", areaVenta=" + areaVenta + ", porcentajeComision="  + ", increme=" + increme + ", vehiculo=" + vehiculo + ", clientes=" + clientes + '}';
    }
    
    
    
    
    // Add clients
    public void crearCliente(String cliente){
        clientes.add(cliente);
    }
    
    //Show clients
    public void showClients(){
        String data = "";
        data = clientes.stream().map((cliente) -> "Cliente: " + cliente + "\n").reduce(data, String::concat);
        Util.mostrar(data);
    }
    
    
    
    // delete clients
    public void deleteClients(String del){
        for (String cliente : clientes) {
            if (cliente.equals(del)) {
                clientes.remove(cliente);
                Util.mostrar("Cliente eliminado exitosamente...");
            }
            else{
                Util.mostrar("Cliente no encontrado! ");
                break;
            }
        }
        
    }
    
    
    // print data
    
    public String imprimirDatos(){
        String data = null;
        data += "Puesto: Vendedor\n"
                + "Teléfono móvil: " + this.telMovil + "\n"
                + "Area de venta: " + this.areaVenta + "\n"
                + "--- Vehículo ---: \n"
                + this.vehiculo.data() + "\n"
                + "--- Clientes ---: \n";
        data += clientes.stream().map((cliente) -> cliente + "\n").reduce(data, String::concat);
            return data;
    }
    
    
}
