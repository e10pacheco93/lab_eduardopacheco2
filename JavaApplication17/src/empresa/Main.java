/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Logica log = new Logica();
        Vendedor vende = new Vendedor();

        Secretario secre = new Secretario("Finanzas", 247001, "Eduardo", "Pacheco", "117320456", "La fortuna", 2, 85858585, 450000);
        Secretario secre1 = new Secretario("IT", 24790002, "Juan", "Rivera", "117320789", "Los Angeles", 5, 85858585, 240000);

        Vehiculo vehi1 = new Vehiculo("GNC-217", "Toyota", "2012");
        Vehiculo vehi2 = new Vehiculo("JRM-320", "Nissan", "2020");
        Vehiculo vehi3 = new Vehiculo("CNG-220", "Hyundai", "2006");

        String[] menuR = {"1. Secretario", "2. Vendedor", "3. Jefe", "4. Salir"};

        String[] menuSec = {"1. Informacion.", "2. Incrementar Salario", "3. Salir"};

        String[] menuVen = {"1. Informacion ", "2. Crear cliente ", "3. Eliminar Cliente ", "4. Cambiar vehiculo ", "5. Incrementar Salario ", "6. Salir"};

        log.registrarSec(secre);
        log.registrarSec(secre1);

        log.registrarVehi(vehi1);
        log.registrarVehi(vehi2);
        log.registrarVehi(vehi3);

        APP:
        while (true) {
            int op = Util.menuOpciones(menuR, "Selecione el rol: ") + 1;
            switch (op) {
                case 1:
                    String ced = Util.leer("Cédula");
                    Secretario sec = log.existeSec(ced);
                    if (sec == null) {
                        if (Util.confirmar("Desea Registrarse? ")) {
                            String nom = Util.leer("Nombre: ");
                            String ape = Util.leer("Apellidos: ");
                            String dire = Util.leer("Dirección: ");
                            int labora = Util.leerInt("Tiempo en laborar: ");
                            int tel = Util.leerInt("Telefono: ");
                            int salario = Util.leerInt("Salario: ");

                            String despacho = Util.leer("Nombre de despacho: ");
                            int telFax = Util.leerInt("Telefon Fax: ");

                            Secretario secNuevo = new Secretario(despacho, telFax, nom, ape, ced, dire, labora, tel, salario);

                            if (!log.registrar(secNuevo)) {
                                Util.error("No se pudo registrar el empleado...");
                                break;
                            } else {
                                log.registrarSec(secNuevo);
                                Util.mostrar("Usuario registrado... ");

                            }
                        } else {
                            break;
                        }
                    }
                    Util.mostrar("Usuario ya existente... ");

                    LES:
                    while (true) {
                        op = Util.menuOpciones(menuSec, "Selecione una opcion: ") + 1;
                        switch (op) {
                            case 1:
                                Util.mostrar(log.infoSec(ced));
                                break;
                            case 2:
                                log.incrementoSec(ced);
                                break;
                            case 3:
                                break LES;
                        }
                    }
                    break;

                case 2:
                    ced = Util.leer("Cédula");
                    Vendedor vend = log.existeVen(ced);
                    if (vend == null) {
                        if (Util.confirmar("Desea Registrarse? ")) {
                            String nom = Util.leer("Nombre: ");
                            String ape = Util.leer("Apellidos: ");
                            String dire = Util.leer("Dirección: ");
                            int labora = Util.leerInt("Tiempo en laborar: ");
                            String tel = Util.leer("Telefono movil: ");
                            int salario = Util.leerInt("Salario: ");

                            String despacho = Util.leer("Nombre de Area de venta: ");
                            int telFax = Util.leerInt("Telefono: ");

                            Util.mostrar(log.getVehiculos());
                            int id = Util.leerInt("Ingrese el Id del vehiculo: ");
                            Vehiculo vehi = log.selectVehiculo(id);

                            Vendedor venNuevo = new Vendedor(tel, despacho, vehi, nom, ape, ced, dire, labora, telFax, salario);

                            if (!log.registrar(venNuevo)) {
                                Util.error("No se pudo registrar el cliente...");
                                break;
                            } else {
                                log.registrarVen(venNuevo);
                                Util.mostrar("Usuario registrado... ");

                            }
                        } else {
                            break;
                        }
                    }
                    Util.mostrar("Usuario ya existente... ");

                    LES:
                    while (true) {
                        op = Util.menuOpciones(menuVen, "Selecione una opcion: ") + 1;
                        switch (op) {
                            case 1:
                                Util.mostrar(log.infoVen(ced));
                                break;
                            case 2:
                                String cliente = Util.leer("Nombre de cliente: ");
                                vende.crearCliente(cliente);
                                
                                break;
                            case 3:
                                vende.showClients();
                                String cli = Util.leer("Nombre del cliente a eliminar: ");
                                vende.deleteClients(cli);
                                break;
                            case 4:
                                Util.mostrar(log.getVehiculos());
                                int id = Util.leerInt("Ingrese el Id del vehiculo: ");
                                Vehiculo vehi = log.selectVehiculo(id);

                                break;
                            case 5:
                                log.incrementoVen(ced);
                                break;
                            case 6:
                                break LES;
                        }
                    }
                    break;
                case 3:
                    break;
                case 4:
                    break APP;

            }

        }

    }

}
