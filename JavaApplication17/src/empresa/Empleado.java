/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

/**
 *
 * @author root
 */
public class Empleado {
    //Atributos
    private String nombre;
    private String apellidos;
    private String cedula;
    private String direccion;
    
    private int tiempoLabora;
    private int telefono;
    private int salario;

    
    
    
    
    
    public Empleado() {
    }

    
    public Empleado(String nombre, String apellidos, String cedula, String direccion, int tiempoLabora, int telefono, int salario) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.direccion = direccion;
        this.tiempoLabora = tiempoLabora;
        this.telefono = telefono;
        this.salario = salario;
    }
    
    
    //Metodos
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTiempoLabora() {
        return tiempoLabora;
    }

    public void setTiempoLabora(int tiempoLabora) {
        this.tiempoLabora = tiempoLabora;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombre=" + nombre + ", apellidos=" + apellidos + ", cedula=" + cedula + ", direccion=" + direccion + ", tiempoLabora=" + tiempoLabora + ", telefono=" + telefono + ", salario=" + salario + '}';
    }
    
    
    // Imprimir datos generales
    public String imprimirDatosEmpleado() {
        String data = "";
        data += "Nombre: " + this.nombre + "\n"
                + "Apellidos: " + this.apellidos + "\n"
                + "Cédula: " + this.cedula + "\n"
                + "Dirección: " + this.direccion + "\n"
                + "Años de antigüedad: " + this.tiempoLabora + "\n"
                + "Teléfono de contacto: " + this.telefono + "\n"
                + "Salario: " + String.valueOf(this.salario) + "\n";
        return data;
    }
    
    
}
