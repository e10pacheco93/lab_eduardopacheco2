/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Secretario extends Empleado{
    private String despacho;
    private int telFax;
    public double increme =  0.05;

    public Secretario() {  
    }
    

    public Secretario(String despacho, int telFax, String nombre, String apellidos, String cedula, String direccion, int tiempoLabora, int telefono, int salario) {
        super(nombre, apellidos, cedula, direccion, tiempoLabora, telefono, salario);
        this.despacho = despacho;
        this.telFax = telFax;
    }

    // Imprimir datos secretario
    public void imprimirDatos() {
        String data = super.imprimirDatosEmpleado();
        data += "Puesto: Secretario\n"
                + "Despacho: " + this.despacho + "\n"
                + "Número de fax: " + this.telFax + "\n";
        JOptionPane.showMessageDialog(null, data);
    }

    // Incrementar salario secretario
    public void incrementarSalario() {
        
    }
    

    public String getDespacho() {
        return despacho;
    }

    public void setDespacho(String despacho) {
        this.despacho = despacho;
    }

    public int getTelFax() {
        return telFax;
    }

    public void setTelFax(int telFax) {
        this.telFax = telFax;
    }

    public double getIncreme() {
        return increme;
    }

    public void setIncreme(double increme) {
        this.increme = increme;
    }

    
    

    @Override
    public String toString() {
        return "Secretario{" + "despacho=" + despacho + ", telFax=" + telFax + ", increme=" + increme + '}';
    }
    
    
}
