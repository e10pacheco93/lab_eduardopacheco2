/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Logica {

    private final Empleado[] empleados;
    private final Secretario[] secretarios;
    private final Vendedor[] vendendores;
    private final Vehiculo[] vehiculos;
    private String txt;

    public Logica() {
        empleados = new Empleado[30];
        secretarios = new Secretario[30];
        vendendores = new Vendedor[30];
        vehiculos = new Vehiculo[5];
    }
    
    

    // Registrar empleado
    public boolean registrar(Empleado emp) {
        for (int i = 0; i < empleados.length; i++) {
            if (empleados[i] == null) {
                empleados[i] = emp;
                txt = "Cliente: " + emp.getNombre() + "\n";
                return true;
            }
        }
        return false;
    }

    public boolean registrarSec(Secretario sec) {
        for (int i = 0; i < secretarios.length; i++) {
            if (secretarios[i] == null) {
                secretarios[i] = sec;
                
                return true;
            }
        }
        return false;
    }
    
    
    
    public boolean registrarVen(Vendedor ven) {
        for (int i = 0; i < vendendores.length; i++) {
            if (vendendores[i] == null) {
                vendendores[i] = ven;
                
                return true;
            }
        }
        return false;
    }
    
    public boolean registrarVehi(Vehiculo vehi) {
        for (int i = 0; i < vehiculos.length; i++) {
            if (vehiculos[i] == null) {
                vehiculos[i] = vehi;
                
                return true;
            }
        }
        return false;
    }
    
    // obtener lista de empleados
    public String getClientes() {
        String txt = "Lista de Clientes\n";
        int num = 1;
        for (Empleado empleado : empleados) {
            if (empleado != null) {
                txt += num + ". " + empleado.getCedula() + " - " + empleado.getNombre() + "\n";
            } else {
                txt += num + ". _____________________\n";
            }
            num++;
        }
        return txt;
    }
    
    
    
    public String getSecretario(){
        String txt = "Lista de Clientes\n";
        int num = 1;
        for (Secretario secretario : secretarios) {
            if (secretario != null) {
                txt += num + ". " + secretario.getCedula() + " - " + secretario.getNombre() + "\n";
            } else {
                txt += num + ". _____________________\n";
            }
            num++;
        }
        return txt;
    }
    
    
    public String getVehiculos(){
        String txt = "Lsita de Vehiculos \n";
        int num = 0;
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo != null) {
                txt += num + ". " + vehiculo.getMarca() + " - " + vehiculo.getModelo() + "\n";
                
            }else{
                txt += num + ". _____________________\n";
            }
            num++;
        }
        return txt;
    }
    
    public Vehiculo selectVehiculo(int id){
        Vehiculo typeVehi;
        for (Vehiculo vehiculo : vehiculos) {
            for (int i = 1; i < vehiculos.length; i++) {
                typeVehi = vehiculos[id];
                JOptionPane.showMessageDialog(null, typeVehi.getMarca());
                return typeVehi;
            }
        }
        return null;
    }
    
    
    
    
    public Empleado existe(String ced) {
        for (Empleado empleado : empleados) {
            if (empleado != null && empleado.getCedula().equals(ced)) {
                txt = "Empleado: " + empleado.getNombre() + "\n";
                return empleado;
            }
        }
        return null;
    }
    
    
    // esiste Secretaria
    public Secretario existeSec(String ced) {
        for (Secretario secretario : secretarios) {
            if (secretario != null && secretario.getCedula().equals(ced)) {
                txt = "Empleado: " + secretario.getNombre() + "\n";
                return secretario;
            }
        }
        return null;
    }
    
    // existe Vendedor
    
    public Vendedor existeVen(String ced) {
        for (Vendedor vendedor : vendendores) {
            if (vendedor != null && vendedor.getCedula().equals(ced)) {
                txt = "Empleado: " + vendedor.getNombre() + "\n";
                return vendedor;
            }
        }
        return null;
    }
    
    public String infoSec(String ced){
        for (Secretario secretario : secretarios) {
            if (secretario != null && secretario.getCedula().contains(ced)) {
                String data = "";
                data += "Nombre: " + secretario.getNombre() + "\n"
                + "Apellidos: " + secretario.getApellidos() + "\n"
                + "Cédula: " + secretario.getCedula() + "\n"
                + "Dirección: " + secretario.getDireccion() + "\n"
                + "Años de antigüedad: " + secretario.getTiempoLabora() + "\n"
                + "Teléfono de contacto: " + secretario.getTelefono() + "\n"
                + "Salario: " + secretario.getSalario() + "\n"
                + "Telefono Fax: " + secretario.getTelFax() + "\n"
                + "Despacho: " + secretario.getDespacho();
                return data;
            }
        }
        return null;
    }
    
    public String infoVen(String ced){
        for (Vendedor vendedor : vendendores) {
            if (vendedor != null && vendedor.getCedula().contains(ced)) {
                String data = "";
                data += "Nombre: " + vendedor.getNombre() + "\n"
                + "Apellidos: " + vendedor.getApellidos() + "\n"
                + "Cédula: " + vendedor.getCedula() + "\n"
                + "Dirección: " + vendedor.getDireccion() + "\n"
                + "Años de antigüedad: " + vendedor.getTiempoLabora() + "\n"
                + "Teléfono de contacto: " + vendedor.getTelefono() + "\n"
                + "Salario: " + vendedor.getSalario() + "\n"
                + vendedor.imprimirDatos();
                        
                return data;
            }
        }
        return null;
    }
    

    
    //Cambiar supervisor
    
    
    // Incrementar Salario Secretario
    public String incrementoSec(String ced){
        for (Secretario secretario : secretarios) {
            if (secretario != null && secretario.getCedula().contains(ced)){
                int totalPorce = (int) (secretario.increme*secretario.getSalario());
                int totalIncre = totalPorce * secretario.getTiempoLabora();
                int total = totalIncre + secretario.getSalario();
                JOptionPane.showMessageDialog(null, total);
                
            }
        }
        
        return null;
    }
    
    // Incrementar Salario Vendedor
    public String incrementoVen(String ced){
        for ( Vendedor vendedor : vendendores) {
            if (vendedor != null && vendedor.getCedula().contains(ced)){
                int totalPorce = (int) (vendedor.increme *vendedor.getSalario()) ;
                int totalIncre = totalPorce * vendedor.getTiempoLabora();
                int total = totalIncre + vendedor.getSalario();
                JOptionPane.showMessageDialog(null, total);
            }
        }
        
        return null;
    }

}
